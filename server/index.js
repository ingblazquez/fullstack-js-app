const express = require("express");
const mongoose = require("mongoose");
var Request = require("request");
const cors = require("cors");
const Post = require("./models/Post");

const REQUEST_URL = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";

mongoose.connect("mongodb://mongo:27017/docker-node-mongo", {
 useNewUrlParser: true
});

function intervalFunc() {
 Request.get(REQUEST_URL, (error, response, body) => {
  if (error) {
   return console.log(error);
  }
  const { hits } = JSON.parse(body);
  saveHits(hits);
 });
}

const saveHits = async hits => {
 for (let index = 0; index < hits.length; index++) {
  const record = hits[index];
  await Post.findOne({ objectID: record.objectID }).then(existingRecord => {
   if (existingRecord) {
   } else {
    new Post({
     ...record,
     deleted: false
    }).save();
   }
  });
 }
};

setInterval(intervalFunc, 3600 * 1000); //one hour interval

const PORT = process.env.PORT || 5000;
const app = express(PORT);
app.use(cors());
require("./routes/postsRoutes")(app);
app.listen(PORT);
